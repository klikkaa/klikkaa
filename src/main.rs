//! Handles game logic

#![deny(missing_docs)]

extern crate gfx_hal as hal;

use std::sync::{Arc, Mutex};
use std::time::Instant;

use hal::pso::{
    BufferDescriptorFormat, BufferDescriptorType, DescriptorSetLayoutBinding, DescriptorType,
    ShaderStageFlags,
};

use hal::buffer::Usage;

use legion::prelude::*;

use core::rendering::create_backend;
use core::rendering::descriptor::DescSetLayout;
use core::rendering::shader::ShaderData;
use core::rendering::state::{BufferState, RendererState};
use core::rendering::{Mesh, Vector3, Vertex};

use core::component::{
    MeshComponent, PipelineComponent, PositionComponent, RotationComponent, TextureComponent,
};
use core::resource::Renderer;
use core::system::{create_lua_system, create_rendering_system};

fn main() {
    let mut game_state = World::new();
    let mut resources = Resources::default();

    let mut last_frame_instant = Instant::now();

    let event_loop = winit::event_loop::EventLoop::new();
    let window_builder = winit::window::WindowBuilder::new()
        .with_inner_size(winit::dpi::Size::Physical(winit::dpi::PhysicalSize::new(
            core::rendering::DEFAULT_DIMENSIONS.width,
            core::rendering::DEFAULT_DIMENSIONS.height,
        )))
        .with_title("Klikkaa!".to_string());

    let graphics_backend = create_backend(window_builder, &event_loop);
    let mut renderer_state = unsafe { RendererState::new(graphics_backend) };

    let image = Arc::new(Mutex::new(renderer_state.load_image("ow")));

    let vertex_shader = ShaderData::load_from_disk("quad_vert").unwrap();
    let fragment_shader = ShaderData::load_from_disk("quad_frag").unwrap();

    let uniform_layout_binding = vec![DescriptorSetLayoutBinding {
        binding: 0,
        ty: DescriptorType::Buffer {
            ty: BufferDescriptorType::Uniform,
            format: BufferDescriptorFormat::Structured {
                dynamic_offset: false,
            },
        },
        count: 1,
        stage_flags: ShaderStageFlags::FRAGMENT,
        immutable_samplers: false,
    }];

    // let shader_data = ShaderData::new(ShaderType::VERTEX, std::fs::read_to_string("data/quad.vert").unwrap(), vec![
    //     ShaderLayout {
    //         binding: 0,
    //         ty: BufferType::Uniform
    //     }
    // ]).unwrap();

    // shader_data.save("quad_vert");

    // let shader_data = ShaderData::new(ShaderType::FRAGMENT, std::fs::read_to_string("data/quad.frag").unwrap(), vec![
    //     ShaderLayout {
    //         binding: 0,
    //         ty: BufferType::Uniform
    //     }
    // ]).unwrap();

    // shader_data.save("quad_frag");

    // Create the layout for the uniform
    let descriptor_layout =
        unsafe { DescSetLayout::new(&renderer_state.device, &uniform_layout_binding) };

    // Create the bound descriptor set
    let quad_desc = unsafe {
        descriptor_layout.create_desc_set(renderer_state.uniform_descriptor_pool.as_mut().unwrap())
    };

    let pipeline = renderer_state.make_pipeline(
        vertex_shader,
        fragment_shader,
        &image.lock().unwrap().get_layout(),
        &quad_desc.get_layout(),
    );

    let w = 0.3;
    let h = 0.3;
    let x = 0.4;
    let y = 0.4;

    let vertices: [Vertex; 6] = [
        Vertex {
            pos: [(w / 2.0) - x, (h / 2.0) + y],
            uv: [0.0, 1.0],
        },
        Vertex {
            pos: [(w / 2.0) + x, (h / 2.0) + y],
            uv: [1.0, 1.0],
        },
        Vertex {
            pos: [(w / 2.0) + x, (h / 2.0) - y],
            uv: [1.0, 0.0],
        },
        Vertex {
            pos: [(w / 2.0) - x, (h / 2.0) + y],
            uv: [0.0, 1.0],
        },
        Vertex {
            pos: [(w / 2.0) + x, (h / 2.0) - y],
            uv: [1.0, 0.0],
        },
        Vertex {
            pos: [(w / 2.0) - x, (h / 2.0) - y],
            uv: [0.0, 0.0],
        },
    ];

    let vertex_buffer = unsafe {
        BufferState::new::<Vertex>(
            &renderer_state.device,
            &vertices,
            Usage::VERTEX,
            &renderer_state.backend.adapter.memory_types,
        )
    };

    game_state.insert(
        (),
        vec![(
            PositionComponent(Vector3::from(0.0, 0.0, 0.0)),
            RotationComponent(glam::Quat::identity()),
            PipelineComponent(Arc::new(Mutex::new(pipeline))),
            TextureComponent(image),
            MeshComponent(Arc::new(Mutex::new(Mesh {
                mesh_data: vertex_buffer,
            }))),
        )],
    );

    use mlua::Lua;
    let lua_state = Lua::new();
    use core::component::{LuaComponent, NameComponent};
    use core::scripting::init_scripting_state;

    init_scripting_state(&lua_state).unwrap();
    let script = r#"
    if our_entity == nil then 
        local name_component = Name("Test")
        local position_component = Position(1, 1, 1)
        local entity = Entity()
        name_component:attach_to(entity)
        position_component:attach_to(entity)
        our_entity = entity
        print("Made entity!")
    else
        print("Entity exists!")
        local name = our_entity:get_name_component()
        local position = our_entity:get_position_component()

        if name == nil then
            print("This is actually kinda tragic :(")
        else
            print(name.name)
            print("Has position.x")
            print(position.x)
            print("Changing position.x")
            position.x = 3
            print(position.x)
        end
    end


"#
    .to_owned();

    game_state.insert(
        (),
        vec![(
            NameComponent("nya".to_owned()),
            LuaComponent(Arc::new(Mutex::new(lua_state)), script),
        )],
    );

    let mut schedule = Schedule::builder()
        .add_system(create_rendering_system())
        .add_system(create_lua_system())
        .build();

    resources.insert(Renderer(renderer_state));

    schedule.execute(&mut game_state, &mut resources);

    event_loop.run(move |event, _, control_flow| {
        *control_flow = winit::event_loop::ControlFlow::Poll;

        match event {
            winit::event::Event::WindowEvent { event, .. } => {
                #[allow(unused_variables)]
                match event {
                    winit::event::WindowEvent::KeyboardInput {
                        input:
                            winit::event::KeyboardInput {
                                virtual_keycode: Some(winit::event::VirtualKeyCode::Escape),
                                ..
                            },
                        ..
                    }
                    | winit::event::WindowEvent::CloseRequested => {
                        *control_flow = winit::event_loop::ControlFlow::Exit
                    }
                    winit::event::WindowEvent::Resized(dims) => {
                        // Tell the renderer to recreate the swapchain
                        let mut renderer_state = <Write<Renderer>>::fetch_mut(&mut resources);
                        renderer_state.0.recreate_swapchain = true;
                    }
                    _ => (),
                }
            }
            winit::event::Event::RedrawRequested(_) => {
                let elapsed = last_frame_instant.elapsed();
                println!(
                    "Delta MS: {:5.2}, FPS: {:6.2}",
                    elapsed.as_secs_f64() * 1000.0,
                    1.0 / elapsed.as_secs_f64()
                );
                last_frame_instant = Instant::now();

                // Run the game systems to update the game
                schedule.execute(&mut game_state, &mut resources);
            }
            winit::event::Event::RedrawEventsCleared => {
                // Ask the window to be redrawn
                let renderer_state = <Read<Renderer>>::fetch_mut(&mut resources);
                renderer_state.0.backend.window.request_redraw();
            }
            _ => (),
        }
    });
}
